# ezaes

Easy to use AES.

## Install

`go get gitlab.com/bdgo/ezaes`

## Example

```go
pw := "secret"
input := "Hello"
fmt.Println(input)
ct, _ := Encrypt(pw, input)
fmt.Println(ct)
pt, _ := Decrypt(pw, ct)
fmt.Println(pt)
```
