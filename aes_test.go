package ezaes

import (
	"testing"
)

//
func TestEncryptDecrypt(t *testing.T) {
	pw := "secret"
	input := "Hello"

	ct, err := Encrypt(pw, input)
	if err != nil {
		t.Error(err)
	}

	pt, err := Decrypt(pw, ct)
	if err != nil {
		t.Error(err)
	}

	if input != pt {
		t.Errorf("Expected: '%s', got '%s'\n", input, pt)
	}

	if len(input) != len(pt) {
		t.Errorf("Lengths are different")
	}
}

func TestEncryptDecryptLongText(t *testing.T) {
	pw := "secret"
	input := "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam et ipsum sapien. Pellentesque lacinia, massa sit amet tincidunt malesuada, risus eros ultrices dolor, vel rhoncus enim lacus vitae eros. Sed ac sem at neque malesuada accumsan at vel metus. Fusce mollis sem eget risus molestie varius. Fusce et maximus sem, porta tempus quam. Aliquam erat volutpat. Aenean eget magna leo. Fusce elit felis, fermentum eu hendrerit ac, ultrices et leo. Nullam ornare interdum rhoncus. Nam et lacus eu eros suscipit pharetra. In vehicula diam nec ipsum tempus, interdum accumsan lectus fermentum. Nulla id feugiat sem. Phasellus sed sem ac ex tempor pulvinar. Sed sed arcu metus. Integer sollicitudin, diam venenatis luctus elementum, mi augue viverra ligula, id lobortis mauris urna eu leo. Vestibulum cursus lectus non purus commodo ultrices."

	ct, err := Encrypt(pw, input)
	if err != nil {
		t.Error(err)
	}

	pt, err := Decrypt(pw, ct)
	if err != nil {
		t.Error(err)
	}

	if input != pt {
		t.Errorf("Expected: '%s', got '%s'\n", input, pt)
	}

	if len(input) != len(pt) {
		t.Errorf("Lengths are different")
	}
}

func TestEncryptDecryptWithKey(t *testing.T) {
	input := "Hello"
	key, err := RandomKey(32)
	if err != nil {
		t.Error(err)
	}

	ct, err := EncryptWithKey(key, input)
	if err != nil {
		t.Error(err)
	}

	pt, err := DecryptWithKey(key, ct)
	if err != nil {
		t.Error(err)
	}

	if input != pt {
		t.Errorf("Expected: '%s', got '%s'\n", input, pt)
	}

	if len(input) != len(pt) {
		t.Errorf("Lengths are different")
	}
}

func TestEncryptDecryptWithKeyLongText(t *testing.T) {
	input := "Proin rhoncus mollis risus, sed rutrum libero interdum eget. Curabitur bibendum et lorem eget porta. Mauris non sagittis dui, at molestie quam. Aliquam euismod a magna sit amet ultricies. Sed sed ante dapibus, vestibulum mauris in, viverra mi. Mauris in nunc pretium, feugiat dolor sollicitudin, rhoncus erat. Maecenas iaculis leo mi, porttitor dapibus nibh efficitur non. Proin feugiat, ipsum auctor sollicitudin cursus, nibh purus mollis massa, eget mollis urna dui nec mi. Vestibulum faucibus ornare nisi, at laoreet leo maximus eget. Etiam ac ante rutrum, consectetur lectus non, malesuada magna. Donec finibus at libero non pellentesque. Donec commodo tristique urna, et vulputate quam elementum eu. Ut et porta ante, a mollis nulla. Proin facilisis euismod tempus."
	key, err := RandomKey(32)
	if err != nil {
		t.Error(err)
	}

	ct, err := EncryptWithKey(key, input)
	if err != nil {
		t.Error(err)
	}

	pt, err := DecryptWithKey(key, ct)
	if err != nil {
		t.Error(err)
	}

	if input != pt {
		t.Errorf("Expected: '%s', got '%s'\n", input, pt)
	}

	if len(input) != len(pt) {
		t.Errorf("Lengths are different")
	}
}
