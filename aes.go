package ezaes

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"encoding/hex"
	"io"
	"strings"

	"golang.org/x/crypto/scrypt"
)

func createKey(password string, salt []byte) ([]byte, error) {
	dk, err := scrypt.Key([]byte(password), salt, 1<<15, 8, 1, 32)

	if err != nil {
		return dk, err
	}

	return dk, nil
}

// Encrypt a string using a password
func Encrypt(password string, input string) (string, error) {
	// Conver the input to a []byte
	plaintext := []byte(input)

	// Create a new salt
	salt := make([]byte, 8)
	rand.Read(salt)

	// Create a new key using the password and the salt
	key, err := createKey(password, salt)
	if err != nil {
		return "", err
	}

	// Create a new block using the key
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	// Create the IV
	ciphertext := make([]byte, aes.BlockSize+len(plaintext))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return "", err
	}

	// Create a stream
	stream := cipher.NewCTR(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], plaintext)

	// Put it all together
	payload := hex.EncodeToString(ciphertext) + "-" + hex.EncodeToString(iv) + "-" + hex.EncodeToString(salt)
	b64Payload := base64.StdEncoding.EncodeToString([]byte(payload))

	return b64Payload, nil
}

// Decrypt a string with a password
func Decrypt(password string, input string) (string, error) {
	// Take it all apart
	b64decoded, err := base64.StdEncoding.DecodeString(input)
	if err != nil {
		return "", err
	}
	decodedString := string(b64decoded)

	// Get all the pieces we need
	pieces := strings.Split(decodedString, "-")

	// Create the new key using the password and the salt
	salt, err := hex.DecodeString(pieces[2])
	if err != nil {
		return "", err
	}
	key, err := createKey(password, salt)
	if err != nil {
		return "", err
	}

	// Create a new block using the key
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	ciphertext, err := hex.DecodeString(pieces[0])
	if err != nil {
		return "", err
	}

	plaintext := make([]byte, len(ciphertext))

	iv, err := hex.DecodeString(pieces[1])
	if err != nil {
		return "", err
	}

	stream := cipher.NewCTR(block, iv)
	stream.XORKeyStream(plaintext, ciphertext[aes.BlockSize:])

	// Trim padding
	plaintext = bytes.Trim(plaintext, "\x00")

	return string(plaintext), nil
}

// EncryptWithKey will encrypt a string with a specified key
func EncryptWithKey(b64key, input string) (string, error) {
	// Conver the input to a []byte
	plaintext := []byte(input)

	// Convert b64key to []byte
	key, err := base64.StdEncoding.DecodeString(b64key)
	if err != nil {
		return "", err
	}

	// Create a new block using the key
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	// Create the IV
	ciphertext := make([]byte, aes.BlockSize+len(plaintext))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return "", err
	}

	// Create a stream
	stream := cipher.NewCTR(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], plaintext)

	// Put it all together
	payload := hex.EncodeToString(ciphertext) + "-" + hex.EncodeToString(iv)
	b64Payload := base64.StdEncoding.EncodeToString([]byte(payload))

	return b64Payload, nil
}

// DecryptWithKey will decrypt with a key
func DecryptWithKey(b64key, input string) (string, error) {
	// Take it all apart
	b64decoded, err := base64.StdEncoding.DecodeString(input)
	if err != nil {
		return "", err
	}
	decodedString := string(b64decoded)

	// Get all the pieces we need
	pieces := strings.Split(decodedString, "-")

	// Convert b64key to []byte
	key, err := base64.StdEncoding.DecodeString(b64key)
	if err != nil {
		return "", err
	}

	// Create a new block using the key
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	ciphertext, err := hex.DecodeString(pieces[0])
	if err != nil {
		return "", err
	}

	plaintext := make([]byte, len(ciphertext))

	iv, err := hex.DecodeString(pieces[1])
	if err != nil {
		return "", err
	}

	stream := cipher.NewCTR(block, iv)
	stream.XORKeyStream(plaintext, ciphertext[aes.BlockSize:])

	// Trim padding
	plaintext = bytes.Trim(plaintext, "\x00")

	return string(plaintext), nil
}

// RandomKey will generate a random key
func RandomKey(size int) (string, error) {
	key := make([]byte, size)

	_, err := rand.Read(key)

	if err != nil {
		return "", err
	}

	b64 := base64.StdEncoding.EncodeToString(key)

	return b64, nil
}
